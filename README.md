# My AWSome Open Distro sandbox

This repository is intendeded for testing and/or showcasing
[Open Distro for Elasticsearch](https://opendistro.github.io/for-elasticsearch/)
features and capabilities.

# Requirements

Basic requirements are:
* Docker
* docker-compose

# Getting started

To get Open Distro for Elasticsearch running just clone or download this repo
and in its folder run:

```
$ docker-compose up -d
```
